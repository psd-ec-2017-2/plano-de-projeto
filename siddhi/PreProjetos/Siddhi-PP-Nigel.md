# Plano de Projeto - PSD/EC - 2017/2
# Atom Plugin for Siddhi
- Nigel Dias


October, 2017

## Basics

1.  What is your preferred e-mail address?
  - nigeldias2012@gmail.com


1. What is your Web Page, Blog, and GitHub?
  - https://github.com/naidjeldias


1. What is your academic background?

  Graduating in Computer Engineering at Federal University of Goiás in Brazil, attending the last semester. Tutorial Education Program (PET) scholarship during a year, presently member of robotic group Pequi Mecânico and internship at América Planos de Saúde enterprise.


1. What other time commitments, such as school work, another job (GSoC is a
full-time activity), or planned vacations will you have during the period of
GSoC?

  This is my last semester of graduation and I intend to start a master's degree in computer science, so during the period of GSoC I will not have other formal commitments or planned vacations.



## Experience

1. What programming languages are you fluent in? Which tools do you
normally use for development?

  I'm more fluent in Java and I have a working knowledge in Python, C ++ and JavaScript. I work with Ubuntu and sometimes with Windows 10. In my internship I am working with Eclipse, Android Studio, Visual Code and Subversion for version control. I have been working with NodeJs and Git for version control in personal projects.

1. Are you familiar with the Javascript programming language? Have you developed
any projects using Javascript?
  Yes. I worked on a class project that consists of developing a chat based on the IRC protocol using Javascript and NodeJs, in the server side. On my internship I work with javascript for hybrid mobile app development. I have adopt Nodejs for server side development in personal projects.

1. Have you developed software in a team environment before? Any projects
with actual users?

  Yes. I have experienced once in a class project and recently I have been working with a friend on a personal project.

1. What kinds of projects/software have you worked on previously? (anything
larger than a class project: academic research, internships, freelance, hobby
projects, etc). In particular, are you (or have you been) involved with any
open source development project? If so, briefly describe the project and the
scope of your involvement.

  Recently I am working on developing a taxi app for Cape Verde, a country on the African continent. Until then the country does not have a taxi request service, so we want to provide this service to the local population. Until now it is just me and a friend of mine developing the Android app and the server side of the application.

  I have also solved an easy hack and submit a patch to LibreOffice project. Easy hacks are small tasks that help new developers to get familiar with LibreOffice project.

## Project

1.  Did you select a project from our list? If yes, which project did you select?

1. Why did you choose this project? If you are proposing a project, give a
description of your proposal, including the expected results.

## References
