# Plano de Projeto - PSD/EC - 2017/2
# Atom Plugin for Siddhi
- Nigel Dias


October, 2017

## Basics

1.  What is your preferred e-mail address?
  - nigeldias2012@gmail.com


1. What is your Web Page, Blog, and GitHub?
  - https://github.com/naidjeldias


## Experience

1. What programming languages are you fluent in? Which tools do you
normally use for development?

1. Are you familiar with the Javascript programming language? Have you developed
any projects using Javascript?

1. Have you developed software in a team environment before? Any projects
with actual users?

1. What kinds of projects/software have you worked on previously? (anything
larger than a class project: academic research, internships, freelance, hobby
projects, etc). In particular, are you (or have you been) involved with any
open source development project? If so, briefly describe the project and the
scope of your involvement.

## Project

1.  Did you select a project from our list? If yes, which project did you select?

1. Why did you choose this project? If you are proposing a project, give a
description of your proposal, including the expected results.

## References
