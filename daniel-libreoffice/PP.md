# Plano de Projeto - PSD/EC - 2017/2
# Solving Annoying Document-Related Bugs on LibreOffice

Daniel Faleiro

October, 2017

## Basics

1.  What is your preferred e-mail address?
  - danielfaleirosilva@gmail.com


1. What is your Web Page, Blog, and GitHub?
  - https://github.com/danielfaleiro


1. What is your nickname on #libreoffice-dev freenode channel?
  - dfsilva


1. What is your academic background?

  I am a Computer Engineer senior undergraduate at Universidade Federal de Goias (UFG) in Brazil. I attended Syracuse University (USA) as an exchange student in 2014 for one academic year. I was also an undergraduate researcher at Illinois Institute of Technology (USA) in Summer 2015 for eight weeks.

  Currently, I am a member of Pequi Mecanico, UFG's robotic research group. And I've been participating in national and international robot competitions. I am part of Pequi Mecanico's IEEE Very Small Size Soccer team and we have achieved 2nd place at IronCup/IWCA in 2017 and 3rd place at Latin American and Brazilian Robotics Competition (LARC) in 2016. I am responsible for the computer vision system in my team.

1. What other time commitments, such as school work, another job (GSoC is a
full-time activity), or planned vacations will you have during the period of
GSoC?

  My only commitment during GSoC would be withteam at Pequi Mecanico. But we have a flexible schedule, specially during summer. I won't be a problem to any GSoC activities.


## Experience

1. What programming languages are you fluent in? Which tools do you normally use for development?

  I am fluent in C++. I also have programmed in Javacript, Java and Python. I currently use Atom, but I have already used Microsoft Visual Studio, Netbeans and Notepad++.

1. Why did you choose LibreOffice as your GSoC project?

  I am a LibreOffice user and I really like it as an office suite. LibreOffice has grown and improved a lot in recent years and it is a very solid and renowned open source project. It also has been helping millions of workers and students in the world to achieve their goals and I would be very glad to help those users to go further by working on LibreOffice project.

1. Have you ever submitted a patch to LibreOffice?

  Yes. I solved an easy hack: tdf#111739 (https://bugs.documentfoundation.org/show_bug.cgi?id=111739).

1. What kinds of projects/software have you worked on previously? (anything larger than a class project: academic research, internships, freelance, hobby projects, etc). In particular, are you (or have you been) involved with any open source development project? If so, briefly describe the project and the scope of your involvement.

  I am one of the owners of our Pequi Mecânico team project. The project's code that is available on GitHub: https://github.com/PEQUI-VSSS/VSSS-EMC. This is the code used by our team at robotic competitions and it includes our computer vision system, along with our strategy algorithm, the robots communication and a user graphic interface. It is written in C++.

  I also worked with a development of an app for both Android and iOS in my internship, but the app has not been released yet. It is a simple app that simulates the cost, the benefits and the area needed to have your own photovoltaic modules according to the user's input data. We used Javascript and React Native in the development.


## Project

1.  Did you select a project from our list? If yes, which project did you select?

  No, I did not choose a project from the list. My plan is to solve annoying document-related bugs that affects LibreOffice.

1. Why did you choose this project? If you are proposing a project, give a
description of your proposal, including the expected results.

  I chose this project because I am picky user myself. I usually have no patient dealing with programs bugs and bad behaviors. If a program does not behave really nice and I can choose another program to do the task, I usually go for the alternative program without a doubt. Therefore, I believe LibreOffice might loose a lot of potential users due those annoying bugs. Nowadays, there is a lot of Office Suites alternatives. My plan is to help to turn LibreOffice even more neat so users can be really glad to choose LibreOffice over the alternatives.

  I've been looking for interface and document-related bugs on Bugzilla. I provide a list with some examples below:

  - Bug 105268 - Auto Fill: The Next Value for "001-001-001" is "001-001-002" Rather than "001-001000" (https://bugs.documentfoundation.org/show_bug.cgi?id=105268)
  - Bug 111773 - Wrong selection after merge cells in an Impress table (https://bugs.documentfoundation.org/show_bug.cgi?id=111773)
  - Bug 111789 - TextBox shadow propeties are not saved to PPTX (https://bugs.documentfoundation.org/show_bug.cgi?id=111789)
  - Bug 55410 - automatically fill in the default user's name into tools->options->user data (https://bugs.documentfoundation.org/show_bug.cgi?id=55410)
  - Bug 111790 - Shadow imported from a PPTX file is not overriden by the settings while saving back to PPTX (https://bugs.documentfoundation.org/show_bug.cgi?id=111790)
  - Bug 55066 - FILEOPEN: flaws in importing 123 files (https://bugs.documentfoundation.org/show_bug.cgi?id=55066)
  - Bug 34965 - Make Impress slides editable during presentation (https://bugs.documentfoundation.org/show_bug.cgi?id=34965)
  - Bug 45617 - Make Impress masterpages copyable (https://bugs.documentfoundation.org/show_bug.cgi?id=45617)
  - Bug 61313 - CONDITIONAL FORMATTING: Icon set should have a color reverse switch (https://bugs.documentfoundation.org/show_bug.cgi?id=61313)
  - Bug 93727 - Date Literal should be supported (https://bugs.documentfoundation.org/show_bug.cgi?id=93727)
  - Bug 95405 (Sidebar-Find-and-Replace) - Sidebar deck for find/search and replace (https://bugs.documentfoundation.org/show_bug.cgi?id=95405)
  - Bug 100370 - Status bar functions - Selection Icon not working properly on left mouse click (https://bugs.documentfoundation.org/show_bug.cgi?id=100370)
  - Bug 105965 - UI request to synchronize 'File > Print' and 'File > 'Export as PDF' default behavior (https://bugs.documentfoundation.org/show_bug.cgi?id=105965)
  - Bug 111769 - Black is set as New color in Area tab when a table with multiple colors in its cells is selected (https://bugs.documentfoundation.org/show_bug.cgi?id=111769)
  - Bug 56676 - EDITING: Tables in databases based on calc are unwriteable - Form Properties show them as writable (https://bugs.documentfoundation.org/show_bug.cgi?id=56676)

  This is not a closed list. Bugs can be added and removed from list according to the priority schedule. I intend to have a conversation with my mentor to decide which bugs are more important and should be resolved first. Then, I will plan a "bug-solver" schedule that includes a study of the code and solving the bug.

## References

BUGZILLA. LibreOffice's BugZilla Main Page. Available at https://bugs.documentfoundation.org/.


LIBREOFFICE EASY HACKS. Easy Hacks C++. Available at https://wiki.documentfoundation.org/Development/EasyHacks/by_Required_Skill/Skill_C%2B%2B.


LIBREOFFICE'S WIKI. Wiki's homepage. Available at https://wiki.documentfoundation.org/Main_Page.


GERRIT. LibreOffice's Gerrit Code Review. Availabe at https://gerrit.libreoffice.org/.
